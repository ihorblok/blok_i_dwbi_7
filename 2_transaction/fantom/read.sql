use education
go


begin transaction

select * 
from products  
where productid > 3

waitfor delay '00:00:10'

select * 
from products  
where productid > 3

commit transaction
