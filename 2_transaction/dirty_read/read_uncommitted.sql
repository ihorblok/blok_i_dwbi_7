use education
go

set transaction isolation level read uncommitted
go

begin transaction
	select city 
	from Products 
	where productid = 1

commit transaction