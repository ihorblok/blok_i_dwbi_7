USE i_blok_library
GO

UPDATE TOP (10) authors
SET url = 'www.'+url
GO  

UPDATE TOP (10) books
SET url = 'www.'+url
GO 

UPDATE TOP (10) publisher
SET url = 'www.'+url
GO 

UPDATE TOP (10) booksauthors
SET seq_no = 2*seq_no
GO 

select * from booksauthors
GO