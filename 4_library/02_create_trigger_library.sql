--************************************

USE i_blok_library
GO

DROP trigger IF EXISTS library_audit
GO
--======================================

CREATE TRIGGER library_audit ON [authors]
AFTER  DELETE, INSERT, UPDATE
AS 
BEGIN
    IF EXISTS (SELECT * FROM DELETED) AND NOT EXISTS(SELECT * FROM INSERTED)
    BEGIN     
      INSERT INTO authors_log
       (author_id_old, name_old, url_old, operation_type, operation_datetime )
      SELECT author_id, name, url, 'D', getdate() FROM DELETED
      WHERE author_id in (SELECT DISTINCT author_id from DELETED)
    END
      IF EXISTS(SELECT * FROM INSERTED) AND NOT EXISTS(SELECT * FROM DELETED)
      BEGIN    
        INSERT INTO authors_log
        (author_id_new, name_new, url_new, operation_type, operation_datetime )
        SELECT author_id, name, url, 'I', getdate() FROM INSERTED
        WHERE author_id in (SELECT DISTINCT author_id FROM INSERTED)
      END
        IF (EXISTS(SELECT * FROM INSERTED) AND EXISTS(SELECT * FROM DELETED))
        BEGIN 
            DECLARE @d VARCHAR(255) = 'U'
            DECLARE @t DATETIME =getdate()
            INSERT INTO authors_log
            (author_id_new, name_new, url_new, author_id_old, name_old, url_old, operation_type, operation_datetime)
            SELECT i.author_id, i.name, i.url, d.author_id, d.name, d.url, @d, @t FROM inserted i join deleted d on i.author_id = d.author_id
        END
END
GO