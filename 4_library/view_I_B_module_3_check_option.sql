use i_b_module_3
go

DROP VIEW IF EXISTS my_view_check
go

-- view whith check

CREATE VIEW [my_view_check] AS
SELECT laptop.model, laptop.serial, product.maker 
FROM I_B_module_3.laptop JOIN I_B_module_3.product ON laptop.id_laptop=product.id_laptop
with check option;
go