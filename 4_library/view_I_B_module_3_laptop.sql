use i_b_module_3
go

-- create view laptop

CREATE VIEW [my_view_laptop] AS
SELECT  [serial],
		[model],
		[speed],
		[ram],
		[hd],
		[dvd_rom],
		[price],
		[screen],
		[date_making],
		[inserted_date],
		[updated_date]
       	FROM [laptop]
GO