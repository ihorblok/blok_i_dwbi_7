USE i_blok_library
GO

DROP TRIGGER IF EXISTS not_del
GO
--*********************
CREATE TRIGGER not_del ON [authors_log]
INSTEAD OF DELETE
AS 
    BEGIN
        raiserror('Deletions not allowed from this table', 16, 1)
        rollback transaction
    END
GO

DELETE FROM authors_log
WHERE operation_id=1
GO
