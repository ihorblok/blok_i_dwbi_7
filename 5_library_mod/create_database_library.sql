DROP DATABASE IF EXISTS i_blok_library
GO

CREATE DATABASE i_blok_library
GO

USE i_blok_library
GO

--******************************
ALTER DATABASE i_blok_library
ADD FILEGROUP DATA
go

-- Add a file to the file group

ALTER DATABASE i_blok_library
ADD FILE (
      NAME = library_file,
      FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\library_file.mdf',
      SIZE = 8MB,
      MAXSIZE = 100MB,
      FILEGROWTH = 5MB
      )
   TO FILEGROUP DATA
GO
--******************************



ALTER DATABASE [i_blok_library]
MODIFY FILEGROUP DATA DEFAULT;
GO

DROP TABLE if EXISTS [booksauthors];
GO

DROP TABLE if EXISTS [books];
GO

DROP TABLE if EXISTS [authors_log];
GO

DROP TABLE if EXISTS [publisher];
GO

DROP TABLE if EXISTS [authors];
GO

-- [authors_log]

CREATE TABLE [authors_log]
(
 [operation_id]       INT NOT NULL IDENTITY (1, 1),
 [author_id_new]      INT NULL ,
 [name_new]           VARCHAR(255) NULL ,
 [url_new]            VARCHAR(255) NULL ,
 [author_id_old]      INT NULL ,
 [name_old]           VARCHAR(255) NULL ,
 [url_old]            VARCHAR(255) NULL ,
 [operation_type]     VARCHAR(255) NOT NULL ,
 [operation_datetime] DATETIME NOT NULL DEFAULT (getdate()),

 CONSTRAINT [PK_authors_log] PRIMARY KEY ([operation_id])
);
GO

-- [publisher]

CREATE TABLE [publisher]
(
 [publisher_id] INT NOT NULL ,
 [name]         VARCHAR(255) NOT NULL ,
 [url]          VARCHAR(255) NOT NULL default ('www.publisher_name.com'),
 [inserted]		  DATE NOT NULL default (getdate()),
 [inserted_by]  VARCHAR(255) NOT NULL default (system_user),
 [updated]      DATE NULL ,
 [updated_by]   VARCHAR(255) NULL ,

  CONSTRAINT [PK_publisher] PRIMARY KEY ([publisher_id]),
  CONSTRAINT uk_publisher UNIQUE ([name])
);
GO

CREATE SEQUENCE library_seq
AS INT
START WITH 1
increment by 1 
GO
-- [authors]

CREATE TABLE [authors]
(
 [author_id]        INT NOT NULL ,
 [name]             VARCHAR(255) NOT NULL ,
 [url]              VARCHAR(255) NOT NULL default ('www.author_name.com'),
 [inserted]         DATE NOT NULL default (getdate()),
 [inserted_by]      VARCHAR(255) NOT NULL default (system_user),
 [updated]          DATE NULL,
 [updated_by]       VARCHAR(255) NULL ,

 CONSTRAINT [PK_authors] PRIMARY KEY ([author_id]),
 CONSTRAINT uk_authors unique ([name])  
);
GO
 

--[books]

CREATE TABLE [books]
(
 [isbn]             VARCHAR(255) NOT NULL ,
 [publisher_id]     INT NOT NULL ,
 [url]              VARCHAR(255) NOT NULL ,
 [price]            NUMERIC(18,2) NOT NULL default (0),
 [inserted]         DATE NOT NULL default (getdate()),
 [inserted_by]      VARCHAR(255) NOT NULL default (system_user),
 [updated]          DATE NULL ,
 [updated_by]       VARCHAR(255) NULL,
 

  CONSTRAINT [PK_books] PRIMARY KEY ([isbn]),
  CONSTRAINT [FK_publisher_id] FOREIGN KEY ([publisher_id])
  REFERENCES [publisher]([publisher_id])
  ON DELETE CASCADE,
  CONSTRAINT uk_books unique ([url]),
  check (price>=0)
);
GO

--[booksauthors]

CREATE TABLE [booksauthors]
(
 [booksauthors_id] INT NOT NULL default(1),
 [isbn]             VARCHAR(255) NOT NULL ,
 [author_id]        INT NOT NULL ,
 [seq_no]           INT NOT NULL default (1),
 [inserted]         DATE NOT NULL default (getdate()),
 [inserted_by]      VARCHAR(255) NOT NULL default (system_user),
 [updated]          DATE NULL ,
 [updated_by]       VARCHAR(255) NULL ,
 
 CONSTRAINT [PK_booksauthors] PRIMARY KEY ([booksauthors_id]),
 CONSTRAINT [FK_isbn] FOREIGN KEY ([isbn])
  REFERENCES [books]([isbn])
  ON DELETE CASCADE,
 CONSTRAINT [FK_author_id] FOREIGN KEY ([author_id])
  REFERENCES [authors]([author_id]) ON DELETE CASCADE,
  CONSTRAINT uk_author_id unique ([author_id]),
  CONSTRAINT uk_isbn unique ([isbn]),
  check (booksauthors_id >=1),
  check (seq_no >=1)
  
  )
GO
