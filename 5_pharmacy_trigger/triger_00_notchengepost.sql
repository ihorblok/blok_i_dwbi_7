USE [triger_fk_cursor]
GO

DROP TRIGGER IF EXISTS tr_prohibition
GO


-- HW5 #3 NOT 00
CREATE TRIGGER not00_nomber ON employee
    AFTER INSERT, UPDATE
    AS   
    IF EXISTS (SELECT * 
            FROM employee em join inserted i on em.identity_number=i.identity_number
            WHERE  i.identity_number like '%00' )
                BEGIN  
                    RAISERROR ('The UPDATE or INSERT imposible', 16, 1 )
                    ROLLBACK TRANSACTION
                END
GO

-- HW5 #3 CODE_MASK
CREATE TRIGGER code_mask ON [medicine]
    AFTER INSERT, UPDATE
    AS   
    IF EXISTS (SELECT * 
            FROM medicine med join inserted i on med.ministry_code =i.ministry_code
            WHERE  i.ministry_code not like '[a-l,n,o,q-z][a-l,n,o,q-z][-][0-9][0-9][0-9][-][0-9][0-9]')
                BEGIN  
                    RAISERROR ('The UPDATE or INSERT imposible like like', 16, 1 )
                    ROLLBACK TRANSACTION
                END
GO

-- HW5 #3 prohibition of modification

CREATE TRIGGER tr_prohibition ON [post]
AFTER  DELETE, INSERT, UPDATE
AS 
    IF (EXISTS (SELECT * FROM DELETED) or EXISTS(SELECT * FROM INSERTED))
    BEGIN 
            RAISERROR ('The UPDATE, INSERT or DELETE imposible! Good luck', 16, 1)
            ROLLBACK TRANSACTION
    END
GO

update medicine
SET ministry_code = 'df-159-25'  
WHERE id = 1;  
GO  

update employee
SET identity_number = '1111111111'  
WHERE id = 3;  
GO  

update post
SET post = 'www'  
WHERE post = 'qqq';  
GO 

SET IDENTITY_INSERT dbo.employee on
GO
INSERT into [employee] ([id], [surname], [name], [midle_name], [identity_number], [passport], [experience], [birthday], [post], [pharmacy_id])
VALUES  (13, 'БЛЬОК', 'ІГОР', 'ВОЛОДИМИРОВИЧ', '1111111100', 'СА445678', 21.0, '1973-03-21', 'SSF', 2)
GO

select * from post


DELETE FROM post --WHERE id = 1