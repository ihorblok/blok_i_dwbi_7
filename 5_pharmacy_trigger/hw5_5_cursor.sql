USE [triger_fk_cursor]
GO

DECLARE @SQLString nvarchar(max)
DECLARE @table nvarchar(max)
DECLARE @table1 nvarchar(max)
DECLARE @table2 nvarchar(max)
DECLARE @time_table VARCHAR(30)
DECLARE @str varchar(30)
DECLARE @id int,
        @surname varchar(30),
        @name char(30),
        @midle_name varchar(30),
        @identity_number char(10),
        @passport char(10),
        @experience decimal(10, 1),
        @birthday date,
        @post varchar(15),
        @pharmacy_id int

set @time_table=convert(varchar, (format(CONVERT (time, getdate()), 'hh\_mm\_ss')))
set @table='(id             int NOT NULL,
            surname         varchar(30) NOT NULL,
            name            char(30) NOT NULL,
            midle_name      varchar(30) NULL,
            identity_number char(10) NULL,
            passport        char(10) NULL,
            experience      decimal(10, 1) NULL,
            birthday        date NULL,
            post            varchar(15) NOT NULL,
            pharmacy_id     int NULL
            )'
set @table1='CREATE TABLE table1_'+@time_table+' '+@table
set @table2='CREATE TABLE table2_'+@time_table+' '+@table


DECLARE employee_backup CURSOR

FOR SELECT [id], [surname], [name], [midle_name], [identity_number],
[passport], [experience], [birthday], [post], [pharmacy_id] FROM [employee]

OPEN employee_backup 
EXECUTE (@table1 + ' ' + @table2)
FETCH NEXT FROM employee_backup INTO  @id, @surname, @name, @midle_name, @identity_number,
@passport, @experience, @birthday, @post, @pharmacy_id 

WHILE @@FETCH_STATUS=0
    BEGIN
            
            SET @str='' 
            SELECT TOP(1) @str=@str + i FROM (VALUES ('table1_'), ('table2_')) d(i)
            ORDER BY newid()  
            SET @SQLString= 'INSERT INTO '+ @str +@time_table+ ' select * from [employee] where id='+convert(nvarchar(5), @id)
            EXECUTE (@SQLString)    
            FETCH NEXT FROM employee_backup INTO  @id, @surname, @name, @midle_name, @identity_number,
            @passport, @experience, @birthday, @post, @pharmacy_id 
    END
CLOSE employee_backup
DEALLOCATE employee_backup
GO
