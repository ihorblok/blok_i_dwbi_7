USE [triger_fk_cursor]
GO

DECLARE @surname varchar(30),  @name varchar(30)
DECLARE @SQLString nvarchar(max), @table_build nvarchar(max)
DECLARE @column INT
DECLARE @column_type varchar(10), @s varchar(10)
DECLARE @i INT
DECLARE @str varchar(15)
DECLARE @name_table varchar(30)
DECLARE @count int

DECLARE employee_clone CURSOR
FOR SELECT [surname], name FROM [employee]

OPEN employee_clone

FETCH NEXT FROM employee_clone INTO @surname, @name
        WHILE @@FETCH_STATUS =0
                BEGIN   
                        SET @SQLString=''
                        SET @count = 1
                        SET @column=ABS(CHECKSUM(NEWID( )))%9+1   
                        SET @name_table=@surname+@name
                        SET @table_build=''
                        SET @str=''
                        SET @SQLString ='DROP TABLE IF EXISTS ' + @name_table + 'CREATE TABLE ' + @name_table+' ('
                                WHILE @count <= @column 
                                        BEGIN 
                                                SET @s=''
                                                SET @str='' 
                                                SET @i=0
                                                WHILE @i<5
                                                        BEGIN
                                                                SET @s=@s+substring('abcdefghijklmnopqrstuvwxyz', (abs(checksum(newid())) % 26) + 1, 1)
                                                                SET @i=@i+1
                                                        END
                                                SELECT TOP(1) @str=@str + j FROM (VALUES('int'),('varchar(30)'),('date'),('datetime')) d(j)
                                                ORDER BY newid() 
                                                SET @table_build=@table_build+ ' ' + @s+'  '+@str+' ,'
                                                SET @count=@count+1
                                        END
                        SET @table_build=LEFT( @table_build, LEN( @table_build) - 1)    
                        SET @table_build=@table_build+')' 
                        EXECUTE (@SQLString+@table_build)
                        FETCH NEXT FROM employee_clone  INTO  @Surname, @Name
                END
CLOSE employee_clone
DEALLOCATE employee_clone
GO