USE [triger_fk_cursor]
GO

-- EMPLOYEE<->POST

DROP TRIGGER IF EXISTS trigger_post_del
GO

CREATE TRIGGER trigger_post_del ON [post]
  INSTEAD OF DELETE
  AS 
  BEGIN

    IF EXISTS (SELECT P.[post] 
                FROM [post] p join [employee] em ON p.[post]=em.[post])
                BEGIN
                  RAISERROR ('The DELETE statement conflicted with the FOREIGN KEY ',16 ,1);
                  ROLLBACK TRANSACTION
                END
  END
  GO

DROP TRIGGER IF EXISTS trigger_post_del
GO

CREATE TRIGGER trigger_employee_u_i ON [employee]
  AFTER INSERT, UPDATE
  AS 
  BEGIN
    if not EXISTS 
    (SELECT 1 FROM INSERTED i JOIN [post] p ON p.post=i.post)
        BEGIN
        RAISERROR ('The UPDATE or INSERT statement conflicted with the FOREIGN KEY', 16, 1 )
        ROLLBACK TRANSACTION
        END
  END
GO

--EMPLOYEE<->PHARMACY 

DROP TRIGGER IF EXISTS trigger_employee_pharmacy
GO
-- set null

CREATE TRIGGER trigger_employee_pharmacy ON [pharmacy]
  INSTEAD OF DELETE
  AS 
  BEGIN
    IF EXISTS (SELECT p.id
                FROM [pharmacy] p join [employee] em ON p.[id]=em.[pharmacy_id])
                BEGIN
                  UPDATE employee
                  SET  [pharmacy_id] = NULL 
                  where pharmacy_id = (SELECT id from deleted)
                END
  END
  GO

CREATE TRIGGER trigger_em_ph_U_I ON [employee]
  AFTER INSERT, UPDATE
  AS 
  BEGIN
    if NOT EXISTS 
    (SELECT 1 FROM INSERTED i JOIN [pharmacy] p ON p.id=i.pharmacy_id)
        BEGIN
        RAISERROR ('The UPDATE or INSERT statement conflicted with the FOREIGN KEY', 16, 1 )
        ROLLBACK TRANSACTION
        END
  END
GO
--PHARMACY<->STREET

CREATE TRIGGER trigger_street_del ON [street]
  INSTEAD OF DELETE
  AS 
  BEGIN

    IF EXISTS (SELECT s.[street] 
                FROM [street] s join [pharmacy] ph ON s.[street]=ph.[street])
                BEGIN
                  RAISERROR ('The DELETE statement conflicted with the FOREIGN KEY ',16 ,1);
                  ROLLBACK TRANSACTION
                END
  END
  GO

CREATE TRIGGER trigger_pharmacy_u_i ON [pharmacy]
  AFTER INSERT, UPDATE
  AS 
  BEGIN
    if not EXISTS 
    (SELECT 1 FROM INSERTED i JOIN [street] s ON s.street=i.street) 
        BEGIN
        RAISERROR ('The UPDATE or INSERT statement conflicted with the FOREIGN KEY', 16, 1 )
        ROLLBACK TRANSACTION
        END
  END
GO

--PHARMACY<->PHARMACY-MEDICINE<->MEDICINE


DROP TRIGGER IF EXISTS trigger_pharmacy_medicine_del
GO 

CREATE TRIGGER trigger_pharmacy_medicine_del ON [pharmacy_medicine]
  INSTEAD OF DELETE
  AS 
  BEGIN

    IF EXISTS (SELECT pm.[pharmacy_id] FROM [pharmacy_medicine] pm join [pharmacy] ph ON pm.[pharmacy_id]=ph.[id]) 
    OR exists (SELECT pm.[medicine_id] FROM [pharmacy_medicine] pm join [medicine] med ON pm.[medicine_id]=med.[id])
                BEGIN
                  RAISERROR ('The DELETE statement conflicted with the FOREIGN KEY (medicine or pharmacy )',16 ,1);
                  ROLLBACK TRANSACTION
                END
  END
  GO

-------

DROP TRIGGER IF EXISTS trigger_pharmacy_medicine_u_i
GO   

CREATE TRIGGER trigger_pharmacy_medicine_u_i ON [pharmacy_medicine]
  AFTER INSERT, UPDATE
  AS 
  BEGIN
    IF EXISTS (SELECT 1 FROM inserted i JOIN [pharmacy] ph ON ph.[id]=i.pharmacy_id ) 
    OR EXISTS (SELECT 1 FROM inserted i join [medicine] med ON med.[id]=i.medicine_id)
        BEGIN
        RAISERROR ('The UPDATE or INSERT statement conflicted with the FOREIGN KEYffffffffffffffff', 16, 1 )
        ROLLBACK TRANSACTION
        END
  END
GO

--MEDICINE<->MEDICINE-ZONE<->ZONE


DROP TRIGGER IF EXISTS trigger_medicine_zone_del
GO 

CREATE TRIGGER trigger_medicine_zone ON [medicine_zone]
  INSTEAD OF DELETE
  AS 
  BEGIN

    IF EXISTS (SELECT mz.[zone_id] FROM [medicine_zone] mz join [zone] z ON mz.[zone_id]=z.[id]) 
    OR exists (SELECT mz.[medicine_id] FROM [medicine_zone] mz join [medicine] med ON mz.[medicine_id]=med.[id])
                BEGIN
                  RAISERROR ('The DELETE statement conflicted with the FOREIGN KEY ',16 ,1);
                  ROLLBACK TRANSACTION
                END
  END
  GO

-------

DROP TRIGGER IF EXISTS trigger_medicine_zone_u_i
GO   

CREATE TRIGGER trigger_medicine_zone_u_i ON [medicine_zone]
  AFTER INSERT, UPDATE
  AS 
  BEGIN
    IF EXISTS (SELECT 1 FROM inserted i JOIN [zone] z ON z.[id]=i.zone_id ) 
    OR EXISTS (SELECT 1 FROM inserted i join [medicine] med ON med.[id]=i.medicine_id)
        BEGIN
        RAISERROR ('The UPDATE or INSERT statement conflicted with the FOREIGN KEY', 16, 1 )
        ROLLBACK TRANSACTION
        END
  END
GO


