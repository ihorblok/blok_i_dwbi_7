use [labor_sql];
go

--1
;WITH pc_product_cte AS
    (
    SELECT maker, model, type 
    FROM product WHERE [type]='PC'
    )
SELECT * FROM pc_product_cte


--2
;WITH pc_product_cte AS
    (
    SELECT maker, model, [type] 
    FROM product WHERE [type] = 'PC'
    ),
maker_PC_CTE AS
    (
    SELECT maker, model, [type] 
    FROM pc_product_cte
    WHERE maker IN ('A', 'B')
    )
SELECT maker, model, [type] FROM maker_PC_CTE
GO

--3
; WITH first_level_CTE(regionID, place_ID, name, PlaceLevel) AS
(
    SELECT g.region_id regionID, g.id place_ID, g.name, 0 
    FROM [geography] g 
    WHERE g.region_id IS NULL
    UNION ALL
    SELECT g.region_id regionID, g.id place_ID, g.name,PlaceLevel +1
    FROM [geography] g JOIN first_level_CTE fl ON g.region_id=fl.place_ID
)

SELECT * FROM first_level_CTE
WHERE PlaceLevel=1
GO

--4
;WITH tree_region_CTE  (regionID, place_ID, name, PlaceLevel)  AS
(
    SELECT g.region_id regionID, g.id place_ID, g.name, 0 
    FROM [geography] g 
    WHERE g.region_id =4
    UNION ALL
    SELECT g.region_id regionID, g.id place_ID, g.name, 1
    FROM [geography] g JOIN tree_region_CTE tr ON g.region_id=tr.place_ID
    
)

SELECT * FROM tree_region_CTE
GO

--5
; WITH num_CTE(num) AS  
    (  
    SELECT num = 1    
    UNION ALL
    SELECT  num + 1 FROM num_CTE WHERE num < 10000
    )
SELECT * FROM num_CTE
OPTION  ( MAXRECURSION 10000 )
GO

--6
; WITH num_CTE(num) AS  
    (  
    SELECT num = 1    
    UNION ALL
    SELECT  num + 1 FROM num_CTE WHERE num < 100000
    )
SELECT * FROM num_CTE
OPTION  ( MAXRECURSION 0 )
GO

--7?



--8
SELECT DISTINCT maker
FROM product
WHERE type='PC'
AND maker NOT IN (
SELECT maker
FROM product
WHERE type='laptop')
GO

--9
SELECT DISTINCT maker
FROM product
WHERE type='PC'
AND maker != ALL (
SELECT maker
FROM product
WHERE type='laptop')
GO

--10
SELECT DISTINCT maker
FROM product
WHERE type='PC'
AND NOT maker= ANY (
SELECT maker
FROM product
WHERE type='laptop')
GO

--11
SELECT DISTINCT maker
FROM product
WHERE type='PC'
AND maker IN (
SELECT maker
FROM product
WHERE type='laptop')
GO

--12
SELECT DISTINCT maker
FROM product
WHERE type='PC'
AND NOT maker != ALL (
SELECT maker
FROM product
WHERE type='laptop')
GO

--13
SELECT DISTINCT maker
FROM product
WHERE type='PC'
AND  maker= ANY (
SELECT maker
FROM product
WHERE type='laptop')
GO

--14
SELECT DISTINCT maker 
FROM product p 
WHERE [type]='pc' AND maker != ALL (SELECT DISTINCT maker FROM Product 
WHERE  model NOT IN (SELECT model FROM pc) AND [type]= 'pc')
GO

--15
SELECT  country ,class
FROM classes 
WHERE country NOT IN (SELECT country FROM classes WHERE country ='Ukraine' 
                      AND  country  NOT IN (SELECT country FROM classes ))
GO

--16


--17