use [labor_sql]
go

--1
SELECT maker, type, speed, hd
FROM pc JOIN product pr ON pc.model=pr.model
WHERE hd<='8'
GO

--2
SELECT DISTINCT maker 
FROM pc JOIN product pr ON pc.model=pr.model
WHERE speed >= 600
GO

--3
SELECT DISTINCT maker
FROM laptop l JOIN product pr ON pr.model=l.model
WHERE speed <= 500 
GO

--4
SELECT a.model model, b.model model, a.hd , a.ram
FROM laptop a JOIN laptop b ON a.hd=b.hd 
WHERE a.model>b.model AND a.ram=b.ram
GO

--5
SELECT a.country, b.[type], a.[type]
FROM classes b JOIN classes a ON b.country=a.country
WHERE b.[type]='bb' AND a.[type]='bc'
GO

--6
SELECT DISTINCT pc.model, pr.maker
FROM pc JOIN product pr ON pc.model=pr.model
WHERE pc.price<600
GO

--7
SELECT distinct prn.model, prc.maker
FROM printer prn JOIN product prc ON prn.model=prc.model
WHERE prn.price>300
GO

--8
SELECT prt.maker, pc.model, pc.price
FROM pc JOIN product prt ON pc.model=prt.model
GO

--9
SELECT DISTINCT prt.maker, prt.model, pc.price
FROM product prt LEFT JOIN pc ON prt.model=pc.model
WHERE prt.type='pc'
GO

--10
SELECT pr.maker, pr.[type] , lp.model, lp.speed
FROM laptop lp JOIN product pr ON lp.model=pr.model
WHERE lp.speed>600
GO

--11
SELECT ships.name, classes.displacement
FROM ships JOIN classes ON ships.class=classes.class
GO

--12
SELECT oc.ship, oc.battle, bat.date 
FROM outcomes oc JOIN battles bat ON oc.battle=bat.NAME
WHERE oc.result!='sunk' 
GO

--13
SELECT sh.name, cl.country
FROM ships sh JOIN classes cl ON sh.class=cl.class
GO

--14
SELECT trip.trip_no, trip.plane, com.name
FROM trip JOIN company com ON trip.id_comp= com.id_comp
WHERE trip.plane='Boeing'
GO

--15
SELECT passenger.name, pass_in_trip.[date]
FROM pass_in_trip JOIN passenger ON pass_in_trip.id_psg=passenger.id_psg
GO

--16
SELECT pc.model, pc.speed, pc.hd
FROM pc JOIN product ON pc.model=product.model
WHERE (hd=10 OR hd=20) AND maker='A'
ORDER BY pc.speed
GO

--17
SELECT *
FROM (SELECT maker, [type] FROM product) pvt
pivot (count ([type])
    FOR [type] IN (pc, laptop,printer)) as tab_piv
GO

--18
SELECT 'average price' AS avg_, [11], [12], [14], [15]
FROM (SELECT price avg_, screen FROM laptop) pvt
pivot (avg (avg_) 
FOR screen IN  ([11], [12], [14], [15])) tab_piv
GO

--19
SELECT p.maker, l.*  
FROM product p 
CROSS APPLY
(SELECT * FROM laptop l WHERE p.model= l.model) l
GO

--20
SELECT *
FROM laptop l 
CROSS APPLY 
(SELECT max(price) max_price 
FROM laptop lt JOIN product p ON lt.model=p.model
WHERE maker = (SELECT maker FROM product p WHERE p.model=l.model )) r

--21
SELECT * FROM laptop lt
CROSS APPLY
(SELECT TOP(1)* FROM laptop l 
WHERE lt.model<l.model OR (lt.model=l.model AND lt.code<l.code)
ORDER BY l.model, l.code ) r
ORDER BY lt.model
GO

--22
SELECT * FROM laptop lt
OUTER APPLY
(SELECT TOP(1)* FROM laptop l 
WHERE lt.model<l.model OR (lt.model=l.model AND lt.code<l.code)
ORDER BY l.model, l.code ) r
ORDER BY lt.model
GO

--23
SELECT r.* FROM
(SELECT DISTINCT [type] from product) p
CROSS APPLY 
(SELECT TOP(3)* FROM product pr WHERE p.[type]=pr.[type]
ORDER BY pr.model) r
GO
