USE [labor_sql];
GO

--1
SELECT row_number() over(ORDER BY id_comp, trip_no) num, trip_no, id_comp
FROM trip
ORDER BY id_comp, trip_no
GO

--2

SELECT row_number() over(partition BY id_comp ORDER BY id_comp, trip_no) num, trip_no, id_comp 
FROM trip
ORDER BY id_comp, trip_no
GO

--3
SELECT model, color, type, price
FROM (SELECT *, RANK() OVER(PARTITION BY type ORDER BY price) pr
      FROM Printer) p
WHERE pr = 1
GO

--4
SELECT maker 
FROM ( SELECT maker, RANK() OVER(PARTITION BY maker ORDER BY model) prank
       FROM Product 
       WHERE type = 'PC') p
WHERE prank = 3
GO

--5 
SELECT DISTINCT price 
FROM (SELECT DENSE_RANK() OVER(ORDER BY price DESC) dr, price FROM PC) p 
WHERE dr=2
GO

--6 
SELECT *, NTILE(3) OVER (ORDER BY (SELECT reverse(SUBSTRING(reverse(rtrim(name)), 1, CHARINDEX(' ', reverse(rtrim(name))) - 1)) lastname
)) name
FROM passenger
GO

--7
SELECT *, 
       ROW_NUMBER() OVER(ORDER BY price DESC) id, 
       count(*) OVER() row_total, 
       NTILE(cast ((SELECT ceiling(count(*)/3.0) FROM pc) as INT)) OVER(ORDER BY price DESC) page_num,
       (SELECT ceiling(count(*)/3.0) FROM pc) page_total
FROM pc
GO

--8
SELECT max_sum, [type], [date], [point]
FROM (SELECT  *, max(suma) over( ORDER BY suma DESC) max_sum 
FROM (SELECT inc suma, [type]='inc', [date], [point] FROM income
      UNION ALL
      SELECT [out] suma, [type]='out', [date], [point] FROM outcome
      UNION ALL
      SELECT inc suma, [type]='inc', [date], [point] FROM income_o
      UNION ALL
      SELECT [out] suma, [type]='out', [date], [point] FROM outcome_o) s ) sm
WHERE max_sum=suma
GO

--9
SELECT *, AVG(price) over() total_price 
      FROM (SELECT *, price-AVG(price) OVER() dif_total_price
      FROM (SELECT *, price-AVG(price) OVER(PARTITION BY speed) dif_local_price FROM pc) ps) p
GO